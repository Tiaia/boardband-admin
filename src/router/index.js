import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/index'
  },
  {
    path: '/',
    component: () => import('../components/common/Base.vue'),
    meta: {
      title: '公共部分'
    },
    children: [
      {
        path: '/index',
        component: () => import('@/components/page/home/Home.vue'),
        meta: {
          title: '系统首页'
        }
      },
      {
        path: '/channel',
        component: () => import('@/components/page/channel/ChannelAdmin.vue'),
        meta: {
          title: '渠道管理'
        }
      },
      {
        path: '/customer',
        component: () => import('@/components/page/customer/CustomerAdmin.vue'),
        meta: {
          title: '客户管理'
        }
      }
    ]
  },
  {
    path: '/login',
    component: () => import('../components/page/Login.vue')
  },
  {
    path: '/error',
    component: () => import('../components/page/Error.vue')
  },
  {
    path: '/404',
    component: () => import('../components/page/404.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
