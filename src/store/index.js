import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // baseUrl: "http://192.168.1.11:9997/",   // 本地测试链接
    baseUrl: "http://101.132.47.109:9997/",
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
